package com.rond.test.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BookDTO {
    private String id;
    private String cvBook;
    private String name;
    private String author;
}
