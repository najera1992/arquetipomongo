package com.rond.test.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.rond.test.dto.BookDTO;
import com.rond.test.model.Book;
import com.rond.test.repository.BookRepository;
import com.rond.test.service.BookService;

@Service
public class BookServiceImpl implements BookService {
	private BookRepository bookRepository;

	public BookServiceImpl(final BookRepository bookRepository) {
		this.bookRepository = bookRepository;
	}

	@Transactional
	@Override
	public BookDTO saveBook(final BookDTO bookDTO) {
		Book book = new Book();
		book.setId(bookDTO.getId());
		book.setName(bookDTO.getName());
		book.setCvBook(bookDTO.getCvBook());
		book.setAuthor(bookDTO.getAuthor());
		book = bookRepository.save(book);
		bookDTO.setId(book.getId());
		return bookDTO;
	}

	@Override
	public List<BookDTO> getBooks() {
		List<BookDTO> books = new ArrayList<>();
		List<Book> booksDO = bookRepository.findAll();
		for (Book book : booksDO) {
			BookDTO bookDTO = new BookDTO();
			BeanUtils.copyProperties(book, bookDTO);
			books.add(bookDTO);
		}
		return books;
	}

	@Override
	@Transactional
	public void deleteBook(final String id) {
		if (id != null) {
			bookRepository.deleteById(id);
		}
	}
}
