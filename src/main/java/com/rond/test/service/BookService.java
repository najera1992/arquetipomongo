package com.rond.test.service;

import java.util.List;

import com.rond.test.dto.BookDTO;

public interface BookService {
	
	BookDTO saveBook(final BookDTO bookDTO);

	List<BookDTO> getBooks();

	void deleteBook(String id);
}
