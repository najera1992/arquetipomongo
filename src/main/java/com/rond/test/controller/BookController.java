package com.rond.test.controller;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.rond.test.dto.BookDTO;
import com.rond.test.service.BookService;

@RestController
@RequestMapping("/api/v1/book")
public class BookController {
	private BookService bookService;

	public BookController(final BookService bookService) {
		this.bookService = bookService;
	}

	@PostMapping
	public ResponseEntity<BookDTO> saveBook(@RequestBody final BookDTO bookDTO) {
		return new ResponseEntity<>(bookService.saveBook(bookDTO), HttpStatus.CREATED);
	}

	@GetMapping
	public ResponseEntity<List<BookDTO>> getBooks() {
		return new ResponseEntity<>(bookService.getBooks(), HttpStatus.OK);
	}

	@DeleteMapping("/{idBook}")
	public ResponseEntity<Void> deleteBook() {
		return new ResponseEntity<Void>(HttpStatus.OK);
	}

}
