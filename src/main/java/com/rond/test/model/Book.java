package com.rond.test.model;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Getter
@Setter
@Document(collection = "book")
public class Book {
    @Id
    private String id;
    private String cvBook;
    private String name;
    private String author;
}
